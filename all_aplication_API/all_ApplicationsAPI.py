import requests
import colorama
from colorama import Fore
import json
import datetime
import time
from hashlib import sha1
from bs4 import BeautifulSoup
import re
import uvicorn
from fastapi import FastAPI


colorama.init(autoreset=True)

# Get AcessToken using Refresh Token
app = FastAPI()

@app.get("/names/{query}")

def names(query):
    names =[]
    country_code = '92'
    number = query
    if number.startswith('+92'):
        search_number=number[3:]
        url = "https://acr2.y0.com/identify"
        payload="[{\"number\":"+search_number+",\"prefix\":"+country_code+"}]"
        headers = {
        'Content-Type': 'application/json',
        'Cookie': '__cfduid=d162f2af6a87dd4a604eda0bfae1ca1691611904868'
        }
        response = requests.request("POST", url, headers=headers, data=payload)

        try:
            if response.status_code==200:
                json_response=response.json()
                new_name=''
                view_caller_name_list=[]
                for names in json_response:
                    new_name=names.get('names')
                for multiple_names in new_name:
                    name = multiple_names.get('name')
                    view_caller_name_list.append(name)
            # return view_caller_name_list
            
        except Exception as ex:
            print("[-]  server_1 Down :( ::  ",str(ex))
    ################################################################
    if '+' in query:
        new_query=query.replace('+','')
    url = "https://api.micallerid.com/callinfov2/subn"
    payload="{\r\n    \"Tel\": "+new_query+"\r\n}"
    headers = {
    # 'authorization': 'Basic eyJ1dG9rZW4iOiJITXRQc3JQVjdiYVM3bVlZMHJ5anBRYkR5RFRmRFR3bkJCczV5WFdCelJIVVpjMWQyeHlJY0dwY2pKTmJSNk9rRkd2TVZpT2NzeGk5TWZORVBLUTJNbGMyeVE4TTMyREF3Tm11eG4ycFh5Q0lNNWhkOEFrTzUyQTBKQVMydmpNUCIsInMiOiIyNTpFRTowRToxQzpGNTo2ODpERDo2QTo0RjozNTpCNzoyRDo5MzozODo2RTo5MzpGNjo2MzpGMjpERSIsImRpIjoiMTI4NTY1YTNhYjc2ZDI3MyJ9',
    'authorization': 'Basic eyJ1dG9rZW4iOiJleUpoYkdjaU9pSklVekkxTmlJc0luUjVjQ0k2SWtwWFZDSjkuZXlKemRXSWlPaUpoWmpreE5tWTNOREkwWVRVeE5qZ3hJaXdpYm1GdFpXbGtJam9pTmpCbFptVm1ZalJtWWpNNFpqUTVNelExWkRZNU1UbGhJaXdpWVdOeUlqb2lNU0lzSW1wMGFTSTZJakU1T0dRek9UaG1MV0ZsTjJVdE5ESm1ZaTA1TnpVMUxUSmhZbVUwTUdVMll6WmhOeUlzSW1semN5STZJa05oYkd4bGNrRndjSE11WTI5dElpd2lZWFZrSWpvaVEyRnNiR1Z5UVhCd2N5NWpiMjBpZlEuNUUxTUpKY1E5aGpNVHJJOVh3ZWRhNWxJaGhQMFd6MGxuQ2x3NV9wNTk3OCIsInMiOiIyNTpFRTowRToxQzpGNTo2ODpERDo2QTo0RjozNTpCNzoyRDo5MzozODo2RTo5MzpGNjo2MzpGMjpERSIsImRpIjoiYWY5MTZmNzQyNGE1MTY4MSJ9',

    'Content-Type': 'application/json; charset=utf-8',
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    res=response.json()
    Names=res.get('Names')
    mi_all_name = []
    if Names :
        for x in Names:
            new_name=x.replace(',','\n')
            mi_all_name.append(new_name)
        
    else:
        pass
    try:
        url = "https://whoscallingme.xyz/data/search_name?country=PK"

        payload='phoneNumber=00'+str(query)
        headers = {
        'Authorization': 'Basic YWEyNTAyOnp1enVBaGgy',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Cookie': 'JSESSIONID=2D9DD48E230AD1658CA461311661BD2B'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        res=response.json()
        if res:
            try:
                result=res.get('result').replace('\\',"").replace("'","")
            except:
                result = ''
        
    except:result = ''
    try:
        url = 'https://datasearch.ml/tmt.php?order_id='+str(query)

        payload={}
        headers = {}
        response = requests.request("GET", url, headers=headers, data=payload)
        if response:
            try:
                res_json_cnic =  response.json()
            except:
                res_json_cnic = ''


    except:res_json_cnic = ''

    return view_caller_name_list,mi_all_name, result  ,res_json_cnic

def view_caller(query):

    country_code = '92'
    number = query
    if number.startswith('+92'):
        search_number=number[3:]
        url = "https://acr2.y0.com/identify"
        payload="[{\"number\":"+search_number+",\"prefix\":"+country_code+"}]"
        headers = {
        'Content-Type': 'application/json',
        'Cookie': '__cfduid=d162f2af6a87dd4a604eda0bfae1ca1691611904868'
        }
        response = requests.request("POST", url, headers=headers, data=payload)

        try:
            if response.status_code==200:
                json_response=response.json()
                new_name=''
                name_list=[]
                for names in json_response:
                    new_name=names.get('names')
                for multiple_names in new_name:
                    name = multiple_names.get('name')
                    name_list.append(name)
            return name_list
            
        except Exception as ex:
            print("[-]  No detail Found ::  ",str(ex))


def miCaller(query):
    if '+' in query:
        new_query=query.replace('+','')
    url = "https://api.micallerid.com/callinfov2/subn"
    payload="{\r\n    \"Tel\": "+new_query+"\r\n}"
    headers = {
    # 'authorization': 'Basic eyJ1dG9rZW4iOiJITXRQc3JQVjdiYVM3bVlZMHJ5anBRYkR5RFRmRFR3bkJCczV5WFdCelJIVVpjMWQyeHlJY0dwY2pKTmJSNk9rRkd2TVZpT2NzeGk5TWZORVBLUTJNbGMyeVE4TTMyREF3Tm11eG4ycFh5Q0lNNWhkOEFrTzUyQTBKQVMydmpNUCIsInMiOiIyNTpFRTowRToxQzpGNTo2ODpERDo2QTo0RjozNTpCNzoyRDo5MzozODo2RTo5MzpGNjo2MzpGMjpERSIsImRpIjoiMTI4NTY1YTNhYjc2ZDI3MyJ9',
    'authorization': 'Basic eyJ1dG9rZW4iOiJleUpoYkdjaU9pSklVekkxTmlJc0luUjVjQ0k2SWtwWFZDSjkuZXlKemRXSWlPaUpoWmpreE5tWTNOREkwWVRVeE5qZ3hJaXdpYm1GdFpXbGtJam9pTmpCbFptVm1ZalJtWWpNNFpqUTVNelExWkRZNU1UbGhJaXdpWVdOeUlqb2lNU0lzSW1wMGFTSTZJakU1T0dRek9UaG1MV0ZsTjJVdE5ESm1ZaTA1TnpVMUxUSmhZbVUwTUdVMll6WmhOeUlzSW1semN5STZJa05oYkd4bGNrRndjSE11WTI5dElpd2lZWFZrSWpvaVEyRnNiR1Z5UVhCd2N5NWpiMjBpZlEuNUUxTUpKY1E5aGpNVHJJOVh3ZWRhNWxJaGhQMFd6MGxuQ2x3NV9wNTk3OCIsInMiOiIyNTpFRTowRToxQzpGNTo2ODpERDo2QTo0RjozNTpCNzoyRDo5MzozODo2RTo5MzpGNjo2MzpGMjpERSIsImRpIjoiYWY5MTZmNzQyNGE1MTY4MSJ9',

    'Content-Type': 'application/json; charset=utf-8',
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    res=response.json()
    Names=res.get('Names')
    all_name = []
    if Names :
        for x in Names:
            new_name=x.replace(',','\n')
            all_name.append(new_name)
        return all_name
    else:
        pass
        # print("MI CALLER name not found")

def number(query):

    # url = "http://chingchong.online/api/v2/resources/simdb?number="+str(query)
    url = 'https://datasearch.ml/tmt.php?order_id='+str(query)

    payload={}
    headers = {}
    response = requests.request("GET", url, headers=headers, data=payload)
    res_json =  response.json()
    print(Fore.GREEN+str(res_json))
    return res_json

def skype_request_flow(query):

  read_refresh_token_file=open('refresh.txt','r')
  refresh_token_from_file=read_refresh_token_file.read()
  read_refresh_token_file.close()

  url = "https://login.live.com/oauth20_token.srf"

  payload='client_id=00000000480BC46C&nopa=1&scope=service%3A%3Alw.skype.com%3A%3AMBI_SSL&refresh_token='+str(refresh_token_from_file)+'&grant_type=refresh_token'
  headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
  }

  response = requests.request("POST", url, headers=headers, data=payload)
  json_response=response.json()
  
  try:
    access_token= json_response.get('access_token')
  except Exception as ex:
    print(Fore.RED+'[-]  Access Token Not Genrated ::  '+str(ex))


  if not access_token:
    print(Fore.GREEN+'\n \n[+]  Genrating Access Token Please Wait . . .')
    read_refresh_token_file=open('new_refresh.txt','r')
    refresh_token_from_file=read_refresh_token_file.read()
    read_refresh_token_file.close()
    url = "https://login.live.com/oauth20_token.srf"

    payload='client_id=00000000480BC46C&nopa=1&scope=service%3A%3Alw.skype.com%3A%3AMBI_SSL&refresh_token='+str(refresh_token_from_file)+'&grant_type=refresh_token'
    headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    json_response=response.json()
    try:
      access_token= json_response.get('access_token')
      print(Fore.GREEN+'[+]  Access Token Genrated Successfully !!! ::  '+Fore.MAGENTA+str(access_token))
      
    except Exception as ex:
      print(Fore.RED+'[-]  Access Token Not Genrated ::  ',str(ex))
    
  try:
    refresh_token=json_response.get('refresh_token')
    try:
      refresh_token_file=open('new_refresh.txt','w')
      refresh_token_file.write(refresh_token)
      refresh_token_file.close()
    except Exception as x:
      print(Fore.RED+"[-]  Issue While Writing file  ::  ",str(x))

  except Exception as refresh_ex:
    print(Fore.RED+"[-]  Refresh token not genrated  ::  ",str(refresh_ex))

  # Get Skype Token using AccessToken
  url = "https://edge.skype.com/rps/v1/rps/skypetoken"
  
  payload = "{\r\n    \"access_token\": \""+access_token+"\",\r\n    \"clientVersion\": \"1419/8.65.0.76\",\r\n    \"scopes\": \"client\",\r\n    \"site_name\": \"lw\"\r\n}"

  headers = {
    'authority': 'edge.skype.com',
    'accept': 'application/json',
    'x-skype-caller': '572381',
    'x-skype-request-id': '343e01bb-937e-4288-912f-d48bdda91e66',
    'content-type': 'application/json; charset=utf-8',
    'user-agent': 'okhttp/3.12.1'
  }

  response = requests.request("POST", url, headers=headers, data=payload)
  res=response.json()
  try:
    skype_token=res.get('skypetoken')
  except Exception as ex:
    print(Fore.RED+"[-]  Issue While Getting Skype Token ::  ", str(ex))
  # User Search skype

  url = "https://skypegraph.skype.com/v2.0/search?searchString="+str(query)+"&requestId=1602492692700&locale=en-US&sessionId=39e05485-16b0-4ff7-9d43-82aef4f82546"

  payload={}
  headers = {
    'accept': 'application/json',
    'x-skypetoken': skype_token,
    'x-skype-client': '1419/8.65.0.76',
    'x-skypegraphservicesettings': '{"experiment":"Default","geoProximity":"disabled","demotionScoreEnabled":"true"}',
    'x-ecs-etag': '"kfqLfnUaBKWhsfXrZuXYjOSLdTkMvT5wjrvt+7ngfOw="',
    'accept-encoding': 'gzip',
    'user-agent': 'okhttp/3.12.1'
  }
  try:
    response = requests.request("GET", url, headers=headers, data=payload)
  except Exception as x:
    print(Fore.RED+"[-]  search Response Issue ::  ",str(x))
  if response.ok:
    user_data=response.json()
    print(Fore.GREEN+str(user_data))
    return json.dumps(user_data)

def twitter(query):
    if "@" in query:
        twitter_url = "http://50.116.35.166/twtr/twlkup/getanatwlup.php?type=email&email="+query+"&map=true"
    else:
        twitter_url = "http://50.116.35.166/twtr/twlkup/getanatwlup.php?type=phone&phone="+query+"&map=true"
    url = twitter_url
    payload = {}
    headers = {
                'Accept-Language': 'application/json'
            }
    response = requests.request("GET", url, headers=headers, data = payload)
    res=response.json()
    for x in res:
        try:
            name=x.get("name")
            username=x.get("screen_name")
            picture=x.get("profile_image_url")
            creation_date=x.get("created_at")
            location=x.get("location")
            description=x.get("description")
            followers_count=x.get("followers_count")
            friends_count=x.get("friends_count")
            favourites_count=x.get("favourites_count")
            dist={"app_name":"Twitter","name ":name,"username ":username,"picture ":picture,"creation ":creation_date,
            "location ":location,"description ":description,"followers  ":followers_count,"friends_count ":friends_count,
            "favourites ":favourites_count}
            print(Fore.LIGHTBLUE_EX+str(dist))
            return dist
        except:
            error=res.get("errors")
            for x in error:
                message=x.get("message")
            dist={"User Detail":message}
            print (Fore.LIGHTBLUE_EX+str(dist))
            return dist

def strava(query):
    url = "https://m.strava.com/api/v3/reset_password"

    payload="{\"email\":\""+query+"\",\"client_secret\":\"3bf7cfbe375675dd9329e9de56d046b4f02a186f\",\"client_id\":2}"
    headers = {
    'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    res=response.json()
    try:
        user_id=res.get('id')
    except:
        print('[-]  User Not Found ')
    if user_id:
        url = "https://m.strava.com/api/v3/athletes/"+str(user_id)+"/profile?hl=en_US"

        payload={}
        files={}
        headers = {
        'authorization': 'access_token de668abe04729652e507522d47a23e400817eb38'
        }

        response = requests.request("GET", url, headers=headers, data=payload, files=files)
        res=response.json()
        dist={"app_name":"STRAVA","data":res}
        print(Fore.MAGENTA+str(dist))
        return dist

def weheartit(query):
    url = "https://api.weheartit.com/api/v2/search/contacts"

    payload = "{\r\n    \"identifiers\": [\r\n        \""+query+"\"\r\n    ],\r\n    \"service\": \"email\"\r\n}\r\n"
    headers = {
    'Authorization': 'Bearer 76dc659f4918f4d89fd2b8b8a43722da977c4ed7af2a4100f7fb733d88e3c93e',
    'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data = payload)
    res= response.json()
    result=res.get("results")
    if result:
        for x in result:
            user_id=x.get("user").get("id")
            username=x.get("user").get("username")
            name=x.get("user").get("name")
            following_status=x.get("user").get("following_status")
            avatar=x.get("user").get("avatar")
            for y in avatar:
                picture=y.get("style")
                if picture =="large":
                    # large_pic=y.get("style")
                    url=y.get("url")
                else:
                    pass
            dist={"app_name":"WeHeartIt","name ":name,"user_id":user_id,"username ":username,"picture ":url,"following_status":following_status}
            print(Fore.YELLOW+str(dist))
            return dist
    else:
        dist={"app_name":"WeHeartIt","Detail":"user not exists"}
        print(Fore.YELLOW+str(dist))
        return dist
#Social_Media == Foursquare
def foursquare(query):
    payload=''
    if '@' in query:
        payload='email='+query+'&oauth_token=H10FLXG5ZXJJIQNV5AH0U2AIUA4HQERYARBIITVZWCBE3KEZ&v=20200710&wsid=195b43a9-8b8b-4d91-9676-5b7cd740d983&m=foursquare&csid=1'
    else:
        payload='phone='+query+'&oauth_token=H10FLXG5ZXJJIQNV5AH0U2AIUA4HQERYARBIITVZWCBE3KEZ&v=20200710&wsid=195b43a9-8b8b-4d91-9676-5b7cd740d983&m=foursquare&csid=1'
    url = "https://api.foursquare.com/v2/users/search"
    # payload='email='+query+'&oauth_token=H10FLXG5ZXJJIQNV5AH0U2AIUA4HQERYARBIITVZWCBE3KEZ&v=20200710&wsid=195b43a9-8b8b-4d91-9676-5b7cd740d983&m=foursquare&csid=1'
    payload=payload
    headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
    }

    response = requests.request("POST", url, headers=headers, data = payload)
    # import pdb;pdb.set_trace();
    res=response.json()
    user_response=res.get("response").get("results")
    try:
        unmatched_phone_Number=res.get("response").get("unmatched").get("phone")
        if unmatched_phone_Number=='+'+query:
            return
    except:
        pass
    
    unmatched_email=res.get("response").get("unmatched").get("email")
    if not unmatched_email:
        user_id=''
        firstName=''
        lastName=''
        gender=''
        countryCode=''
        homeCity=''
        photo=''
        contact=''
        user=''
        try:
            for x in user_response:
                try:
                    user=x.get("user")
                except:
                    print('Social_Media No user Found')
                    return
                if user:
                    user_id=user.get('id')
                    try:
                        firstName=user.get('firstName')
                    except:
                        firstName=''
                    try:
                        lastName=user.get('lastName')
                    except:
                        lastName=''
                    try:
                        gender=user.get('gender')
                    except:
                        gender=''
                    try:
                        countryCode=user.get('countryCode')
                    except:
                        countryCode=''
                    try:
                        homeCity=user.get('homeCity')
                    except:
                        homeCity=''
                    try:
                        bio=user.get('bio')
                    except:
                        bio=''
                    try:
                        photo=user.get('photo')
                    except:
                        photo=''
                    prefix=photo.get('prefix')
                    suffix=photo.get('suffix')
                    try:
                        profile_picture=str(prefix)+str(suffix)
                    except:
                        profile_picture=''
                    try:
                        contact=user.get('contact')
                    except:
                        contact=''
                    twitter = ''
                    twitter_dict={}
                    try:
                        twitter=contact.get('twitter')
                        if twitter :
                            twitter_dict = {"Twitter":'https://www.twitter.com/'+str(twitter)}
                    except:
                        twitter_dict=''
                    facebook = ''
                    face_dict={}
                    try:
                        facebook=contact.get('facebook')
                        if facebook :
                            face_dict = {"Facebook":'https://www.facebook.com/'+str(facebook)}
                    except:
                        face_dict=''
                    dist={
                        "app_name":"Social_Media",
                        "user_id":user_id,
                        "firstName":firstName,
                        "lastName":lastName,
                        "gender":gender,
                        "countryCode":countryCode,
                        "homeCity":homeCity,
                        "bio":bio,
                        "profile_picture":profile_picture
                        # "twitter":twitter,
                        # "facebook":facebook
                    }

                print(Fore.LIGHTGREEN_EX+str(dist))
                if face_dict!= {}:
                    print(Fore.MAGENTA+str(face_dict))
                if twitter_dict!= {}:
                    print(Fore.CYAN+str(twitter_dict))
                return dist,face_dict,twitter_dict
        except:
            dist={"app_name":"Social_Media","Detail":"user not exists"}
            print(dist)
            return dist
    else:
        dist={"app_name":"Social_Media","Detail":"user not exists"}
        print(dist)
        return dist

# ADDIDAS APPLICATION

SECRET_DICT = {
    # old_app_secret
    # "APP_SECRET": "T68bA6dHk2ayW1Y39BQdEnUmGqM8Zq1SFZ3kNas3KYDjp471dJNXLcoYWsDBd1mH",
    "APP_SECRET":"668bA6dHk2ayW1Y39BQdInUmGqN8Zq1SFZ3kMas3RYDjp571dONXLcoYWsDBd2mB",
    "APP_KEY": "com.runtastic.android",
    "sessionCookie": "_runtastic_appws_session",
}
HEADERS = {
    # "X-App-Version": "6.9.2",
    "X-App-Version": "11.21",
    "X-App-Key": "com.runtastic.android",
    "X-Auth-Token": "",
    "Content-Type": "application/json",
    "X-Date": "",
}
def try_to_parse_time(from_time):
    # if not a format timestamp return 0
    if from_time.isdigit():
        if len(from_time) == 10:
            return str(int(from_time) * 1000)
        elif len(from_time) == 13:
            return from_time
        else:
            return "0"
    try:
        from_time = time.mktime(datetime.datetime.strptime(from_time, "%Y-%m-%d").timetuple())
        return str(int(from_time) * 1000)
    except:
        return "0"


def make_auth_token(appKey, appSecret, str_now):

    s = f"--{appKey}--{appSecret}--{str_now}--"
    auth_token = sha1(s.encode("utf-8")).hexdigest()
    return auth_token


def make_request_header(header):
    
    SECRET_DICT = {
        
        "APP_SECRET":"668bA6dHk2ayW1Y39BQdInUmGqN8Zq1SFZ3kMas3RYDjp571dONXLcoYWsDBd2mB",
        "APP_KEY": "com.runtastic.android",
        "sessionCookie": "_runtastic_appws_session",
    }
    str_now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    auth_token = make_auth_token(
        SECRET_DICT["APP_KEY"], SECRET_DICT["APP_SECRET"], str_now
    )
    header["X-Date"] = str_now
    header["X-Auth-Token"] = auth_token
  

    return header


def addidas(query):

    headers = make_request_header(HEADERS)
    url = "https://appws.runtastic.com/webapps/services/auth/v2/login/runtastic"
    

    payload="{\r\n    \"clientId\": \"Q4bab1a87ebbcc40fdd44385e5924f620a93d7eea91ebcaf7b298f6d8f70db70\",\r\n    \"clientSecret\": \"668bA6dHk2ayW1Y39BQdInUmGqN8Zq1SFZ3kMas3RYDjp571dONXLcoYWsDBd2mB\",\r\n    \"grantType\": \"password\",\r\n    \"me\": {\r\n        \"countryCode\": \"PK\",\r\n        \"email\": \"shanzasohail2020@gmail.com\",\r\n        \"locale\": \"en\",\r\n        \"serviceRegion\": \"default\",\r\n        \"timeZone\": \"Asia/Oral\"\r\n    },\r\n    \"password\": \"Qwerty123#@!\",\r\n    \"username\": \"shanzasohail2020@gmail.com\"\r\n}"
    # payload = "{\r\n    \"clientId\": \"Q4bab1a87ebbcc40fdd44385e5924f620a93d7eea91ebcaf7b298f6d8f70db70\",\r\n    \"clientSecret\": \"668bA6dHk2ayW1Y39BQdInUmGqN8Zq1SFZ3kMas3RYDjp571dONXLcoYWsDBd2mB\",\r\n    \"grantType\": \"password\",\r\n    \"me\": {\r\n        \"countryCode\": \"PK\",\r\n        \"email\": \"shanzasohail2020@gmail.com\",\r\n        \"locale\": \"en\",\r\n        \"serviceRegion\": \"default\",\r\n        \"timeZone\": \"Asia/Oral\"\r\n    },\r\n    \"password\": \"Qwerty123#@!\",\r\n    \"username\": \"shanzasohail2020@gmail.com\"\r\n}" 
    
    headers=headers

    response = requests.request("POST", url, headers=headers, data=payload)
    res=response.json()
    access_token=res.get('accessToken')

    url = "https://hubs.runtastic.com/users/v1/search"

    payload="{\r\n    \"data\": {\r\n        \"attributes\": {\r\n            \"page\": {\r\n                \"number\": 1,\r\n                \"size\": 100\r\n            },\r\n            \"query\": [\r\n                \""+query+"\"\r\n            ],\r\n            \"search_attribute\": \"any_email\"\r\n        },\r\n        \"id\": null,\r\n        \"relationships\": null,\r\n        \"type\": \"user_search\"\r\n    }\r\n}"
    
    headers["authorization"] = 'Bearer '+access_token
    headers=headers
    response = requests.request("POST", url, headers=headers, data=payload)
    try:
        if response.status_code==201:
            res=response.json()
            included=res.get('included')
            for x in included:
                user_id=x.get('id')
                userinfo=x.get('attributes')
        dist={"app_name":"Runtastic","user_id ":user_id,"user_info":userinfo}
        print(Fore.GREEN+str(dist))

    except:
        dist={"app_name":"Runtastic","Detail":"user not exists"}
        print(Fore.GREEN+str(dist))
        return dist

# Addidas end

def nike(query):

  url = "https://unite.nike.com/tokenRefresh?platform=android&browser=uniteSDK&mobile=true&native=true&uxid=com.nike.sport.running.droid.3.8&locale=en_US&osVersion=25&sdkVersion=3.1.1&backendEnvironment=identity"

  payload = "{\r\n    \"client_id\": \"WLr1eIG5JSNNcBJM3npVa6L76MK8OBTt\",\r\n    \"grant_type\": \"refresh_token\",\r\n    \"refresh_token\": \"eyJhbGciOiJSUzI1NiIsImtpZCI6ImE5ZjNhOThmLThiMTItNGIwYy1hNTVmLWFiNTVhMWM3MzhmYnNpZyJ9.eyJ0cnVzdCI6MTAwLCJpYXQiOjE2MTQyMzQwNTcsImV4cCI6MTY0NTc3MDA1NywiaXNzIjoib2F1dGgyaWR0IiwianRpIjoiMTBhMGIxYzEtMGNkOS00MmQ4LWIyODgtNzE4ZTk1NGQ1NmFhIiwibGF0IjoxNjE0MjM0MDU3LCJhdWQiOiJvYXV0aDJpZHQiLCJjbGkiOiJXTHIxZUlHNUpTTk5jQkpNM25wVmE2TDc2TUs4T0JUdCIsInN1YiI6ImNjMWI0ZDFiLWJmODItNDU5MC1hMGJhLTg0OGQzNTYwMTMxYiIsInNidCI6Im5pa2U6cGx1cyJ9.LAIRV6Pi2iPz5usbsRWsrrqAwZawqRflop5Igf6D4XW0ciQqOzNEGRQtL-yDIkewaCL1-x9VauyVeBVI6XzuSW5t9BJMNWb7BEtxRXFvZ72PZslbzgNl7c89OPFFb2G3UOjaSTjYIOQiM_46k682qMVvCwX5h9T45b-G7vCwgsS4aOavC265F5U8MA5xo43zxawQ6Dkidoey3S0RMNTEgalxnSCU5sA3SkMdY3Jo0FJM4gB-30ycWbUaV4SMNSDYGhrzFLgyfGkkgfvY7jDHHlNH7tPdfssPsnE4Qu5-jnPPpC4EhD__gbnaKfbE4cQ8DbIIpn1vPtxp35Mx9WX6Vg\"\r\n}"
  headers = {
    'authority': 'unite.nike.com',
    'cache-control': 'no-cache',
    'user-agent': 'NRC/4.2.3 (com.nike.sport.running.droid; build: 1716127345; samsung, SM-N976N; Android 7.1.2; scale: 2.0)',
    'content-type': 'application/json; charset=UTF-8',
    'accept-encoding': 'gzip',
    'x-newrelic-id': 'VQYGVF5SCBAEUFdTBwQBVw==',
  }

  response = requests.request("POST", url, headers=headers, data=payload)
  res =  response.json()
  access_token= res.get('access_token')





  url = "https://api.nike.com/nsl/user/search?app=com.nike.sport.running.droid&format=json&searchstring="+str(query)+"&startIndex=1&count=5"

  payload={}
  headers = {
    'appid': 'com.nike.sport.running.droid',
    'app_version': '3.6.0',
    'authorization': 'Bearer '+str(access_token),
    'nike-api-caller-id': 'nike:NRC:android:3.6.0',
    'x-b3-traceid': 'd634989e90db6b3a',
    'user-agent': 'NRC/3.6.0 (com.nike.sport.running.droid; build: 1715849990; samsung, SM-G930L; Android 7.1.2; scale: 2.0)',
    'shared_feature_version': 'Android 85.0.0',
    'accept-encoding': 'gzip',
    'x-newrelic-id': 'VQYGVF5SCBAEUFdTBwQBVw==',
  }

  try:
      response = requests.request("GET", url, headers=headers, data=payload)
  except Exception as x:
      print("Response Exception ::    "+str(x))

  res=response.json()
  try:
    serviceResponse = res.get('serviceResponse').get('body').get('PaginatedCollection').get('returnObject')
    if serviceResponse:
        dist = {'app_name':'Nike','user_info':serviceResponse}
        print(Fore.CYAN+str(dist))
        return dist
      
  except:
    print("NIKE JSON Response Issue")
  

def olx(query):
    # url = "https://api.olx.com.pk/api/v1/users/find"

    # payload="{\r\n    \"data\": {\r\n        \"identifier\": \""+query+"\"\r\n    }\r\n}"
    # headers = {
    # 'User-Agent': 'android 14.17.004 olxpk',
    # 'Authorization': 'Bearer eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCIsImtpZCI6ImViT21QTmlrIn0.eyJncmFudF90eXBlIjoicGFzc3dvcmQiLCJjbGllbnRfdHlwZSI6ImFuZHJvaWQiLCJ0b2tlbl90eXBlIjoicmVmcmVzaFRva2VuIiwibm90aWZpY2F0aW9uX2h1Yl9pZCI6ImV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpwWVhRaU9qRTJNVEF6TmpRNE5qUXNJbUp5WVc1a0lqb2liMng0SWl3aVkyOTFiblJ5ZVVOdlpHVWlPaUp3YXlJc0luVnpaWEpKWkNJNk1URXpOamcwTkRBeGZRLmNRWmhfd1RKZE5jZEFJX18zZEJ1a2duLXNLdnFaSk8tRDhoQzdwNHZuMG8iLCJjaGF0X3Rva2VuIjoiZXlKaGJHY2lPaUpTVXpVeE1pSXNJblI1Y0NJNklrcFhWQ0lzSW10cFpDSTZJbVZpVDIxUVRtbHJJbjAuZXlKMGIydGxibDkwZVhCbElqb2lZMmhoZEZSdmEyVnVJaXdpYVdGMElqb3hOakV3TXpZME9EWTBMQ0poZFdRaU9pSnZiSGh3YXlJc0ltbHpjeUk2SW05c2VDSXNJbk4xWWlJNklqRXhNelk0TkRRd01TSXNJbXAwYVNJNkltSmpaV0V5TURCak0yWTVaVGxoWXpRNU1qUXlOVEptTldNNU5EYzJNMll5TXpZeU1URXlNekFpZlEuQkRNUTNLQ3dzb2JBRHlkdnZOZjFpa3ZldFp3b1lHcTVhVDE0WW9BNElZSzE4clJnZmZfMGZFQU1qUjZicDJ5ekc1WlEwc0tkS0tRMEZlVXhnMHJGVXdLcldPNzJweFotS3JTSDlTeTdqMXdGdXBWZE5kdzNCVW1Zb3Y3VmxoaklZN1ZpbDJ0bW1kZDRib0hCcTVJYzYyZDBENTBROHZKY2R6MkU4OW9OWTRuRm44YU90NUEzeGRfT2tqbERKcDhWYU83eFd4Qkg2NGpPbEFLTUlySi1IMEt6WndmSk41UmpKV1FYeGF6WUFQUFFxcGJPbGRSSEJKOHpHOHo5eGt1bTRtQ2VqdU00alAtLV81VTBiSk1tS2FBUk8xaHRUYUxKV0JFdTI4MVlaX0NVeTVxeUFjZmY1REFFQVR6X2RIYkd5bmxxWVZOT3RzLUo2NmVwQkdWR213IiwicmVhY3RpdmF0ZWQiOmZhbHNlLCJpYXQiOjE2MTAzNjQ4NjQsImV4cCI6MTYxODQwMDA2NCwiYXVkIjoib2x4cGsiLCJpc3MiOiJvbHgiLCJzdWIiOiIxMTM2ODQ0MDEiLCJqdGkiOiJiY2VhMjAwYzNmOWU5YWM0OTI0MjUyZjVjOTQ3NjNmMjM2MjExMjMwIn0.FpfQrJ-KMPrQnskhHgtydXOZ-L_qcqOaBUqZK6d1-Vnr_CEXrwCwKK0U4KJvb0qGaPFARinowoxz1eq4t7AURY4LZvRhroKgM5DPiVNtwqkp5_7CPGc8YSlBqUedvXHBRLiRh_5Me8-IyJfZn7AD_f9UdxFfe0EEwla3U0eXeLWAYfvVRSy2fPvSKtb1Mta51LpbO3wwZGcttaNoGMo7fwasZ82Koyv5iKuPu7AytQNuqhs3KFLj3z9heIZlPman-5Pez5oidl8yyCcViGWBvhRnIxvFdKS_k95k2sraQWwWwmGHjPRJgZKRHKNSlbuOT2Ey9WNlycPTNI60s942xg',
    # 'Content-Type': 'application/json; charset=UTF-8',
    # 'Host': 'api.olx.com.pk',
    # 'Connection': 'Keep-Alive',
    # 'Accept-Encoding': 'gzip',
    # }
    url = "https://getanaolx2.osintcenter.org/query/"+str(query)

    payload={}
    headers = {}


    try:
        response = requests.request("GET", url, headers=headers, data=payload)
    except Exception as res_except:
        print("[-] Response Exception ::  ",str(res_except))
    res=response.json()
    try:
        
        user_Image = res.get('user_detail').get('user_Image')
        dist={'app_name':'olx','user_info':res,"user_image":user_Image} 
        print(Fore.YELLOW+str(dist))
        return dist
    except:
        info = res.get('user_detail')
        dist={'app_name':'olx','user_info':info} 
        print(Fore.YELLOW+str(dist))
        return dist
    # try:
    #     error=res.get('error')
    #     if error:
    #         print("[-]    User Not Found !    ")
    # except Exception as x:
    #     print("[-]    User Not Found !    ",str(x))

    # if not error:
    #     data=res.get('data')
    # try:
    #     images_data = data['images']
    # except:
    #     images_data = []
                        
    # if images_data!=[]:
    #     ad_images = [single_dict.get('url') for single_dict in images_data if single_dict.get('url')]
    # else:
    #     ad_images=[]

    # array_rec=['anonymous','avatar_id','badges','has_phone','images','is_banned','is_business','is_phone_visible','lang',
    #             'locations','name_provided']
    # for rec in array_rec:
    #     try:
    #         del data[rec]
    #     except:
    #         pass  

def true_caller(query):
        
    proxy = {'https':'http://akhan65:XvVhKp3W@51.89.131.240:29842'}
    headers = {
        'authority': 'webapi-noneu.truecaller.com',
        'sec-ch-ua': '^\\^',
        'accept': 'application/json, text/plain, */*',
        
        'authorization': 'Bearer a1w0e--Qb-J42VJ-HrBDuTIjjMSOW9wft_mr2FHziXxrZBYdNQDDApkyV624Z_Ss',
        'sec-ch-ua-mobile': '?0',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36',
        'origin': 'https://www.truecaller.com',
        'sec-fetch-site': 'same-site',
        'sec-fetch-mode': 'cors',
        'sec-fetch-dest': 'empty',
        'referer': 'https://www.truecaller.com/',
        'accept-language': 'en-US,en;q=0.9',
        'if-none-match': 'W/^\\^512-/oaD0Ki55/ihtOFVRF9UvqK1tjc^\\^',
    }

    params = (
        ('countryCode', 'pk'),
        ('q', str(query)),
    )
    response = requests.get('https://webapi-noneu.truecaller.com/search', headers=headers, params=params,proxies=proxy)
    res_json = response.json()
    print(Fore.MAGENTA+str(res_json))
    return res_json   

def fit_bit(query):
    url = "https://android-api.fitbit.com/oauth2/token?session-data=%7B%22os-name%22%3A%22Android%22%2C%22os-version%22%3A%227.1.2%22%2C%22device-model%22%3A%22SM-N976N%22%2C%22device-manufacturer%22%3A%22samsung%22%2C%22device-name%22%3A%22%22%7D"

    payload='password=uk123%23%40!&scope=activity%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight%20mfa_ok&username=shanzasohail2020%40gmail.com&grant_type=password'
    headers = {
    'authority': 'android-api.fitbit.com',
    'authorization': 'Basic MjI4VlNSOjQ1MDY4YTc2Mzc0MDRmYzc5OGEyMDhkNmMxZjI5ZTRm',
    'content-type': 'application/x-www-form-urlencoded',
    'accept-encoding': 'gzip',
    'user-agent': 'okhttp/4.7.2'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    res_json = response.json()
    access_token = res_json.get('access_token')

    url = "https://android-cdn-api.fitbit.com/1/friend-finder/email/matches.json"

    payload='email='+str(query)
    headers = {
    'authority': 'android-cdn-api.fitbit.com',
    'accept-locale': 'en_US',
    'user-agent': 'Dalvik/2.1.0 (Linux; U; Android 7.1.2; SM-N976N Build/QP1A.190711.020; Scale/2.00; supportsImages={webp};)',
    'x-app-release-version': '3.37.1',
    'x-app-version': '20243550',
    'authorization': 'Bearer '+ str(access_token),
    'content-type': 'application/x-www-form-urlencoded'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    res_json = response.json()
    data = res_json.get('data')
    if data:
        for info in data:
            try:
                fitbit_user_info = info.get('attributes')
            except:
                fitbit_user_info =''
    else:
        fitbit_user_info =''

    dist = {'app_name':'FIT BIT','user_info':fitbit_user_info}
    print(Fore.YELLOW+str(dist))
    return dist

def whosCalling(query):
    url = "https://whoscallingme.xyz/data/search_name?country=PK"

    payload='phoneNumber=00'+str(query)
    headers = {
    'Authorization': 'Basic YWEyNTAyOnp1enVBaGgy',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Cookie': 'JSESSIONID=2D9DD48E230AD1658CA461311661BD2B'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    res=response.json()
    result=res.get('result').replace('\\',"")
    print(Fore.GREEN+str(result))
    return result

def duolingo(query):
    url = "https://android-api-cf.duolingo.com/users/search"

    payload='page=1&per_page=10&q='+str(query)
    headers = {
    'authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjYzMDcyMDAwMDAsImlhdCI6MCwic3ViIjo4MDI2NTA5NDJ9.diA0U_pix5ey45Cm-L3Hf3Zqp7GZKhRiL9rs2LjzwWc',
    'Content-Type': 'application/x-www-form-urlencoded',
    # 'Cookie': 'wuuid=48f3eb02-85e5-41cd-a272-201f4b756416; jwt_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjYzMDcyMDAwMDAsImlhdCI6MCwic3ViIjo4MDI2NTA5NDJ9.diA0U_pix5ey45Cm-L3Hf3Zqp7GZKhRiL9rs2LjzwWc'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    res_json = response.json()
    try:
        user_info = res_json.get('users')
        dist={'app_name':'duolingo','user_info':user_info} 
        print(Fore.BLUE+str(dist))
        return dist
    except:
        pass

def vk(query):
    
  url = "https://api.vk.com/method/account.searchContacts"

  payload='v=5.119&https=1&fields=profile_link%2Cphoto_50%2Cphoto_100%2Cphoto_200%2Ccareer%2Ccity%2Ccountry%2Ceducation%2Cverified%2Ctrending%2Cis_friend&lang=en&search_only=1&count=1&contacts=%7B%22phone%22%3A%7B%22contacts%22%3A%5B%5B%22%2B'+str(query)+'%22%5D%5D%7D%7D&need_mutual=1&access_token=2a2ffca23ceecbd026f5d2f7443586beebb2e2c3cbd54ddf96fb348ee3d6061df0e115a06e804e5b36df2'
  headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
  }

  response = requests.request("POST", url, headers=headers, data=payload)
  res_json = response.json()
  try:
    user_info = res_json.get('response').get('found')
    print(user_info)
  except:
    pass

def eyecon(query):
    
    if '+' in query:
        new_query=query.replace('+','')
    else:
        new_query = query
    

    url = "https://api.eyecon-app.com/app/getnames.jsp?cli="+str(new_query)+"&lang=en&is_callerid=true&is_ic=true&cv=vc_365_vn_3.0.365_a&requestApi=okHttp&source=Other"

    payload={}
    headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
    'accept': 'application/json',
    'e-auth-v': 'e1',
    'e-auth': 'b9b05235-7853-4e65-b8e8-015ff984d700',
    'e-auth-c': '33',
    'e-auth-k': 'PgdtSBeR0MumR7fO',
    'e-checksum': 'ZIv8tFHeLh4aVa0c7nu3/DMLtKc=',
    'accept-charset': 'UTF-8',
    'content-type': 'application/x-www-form-urlencoded; charset=utf-8',
    'Host': 'api.eyecon-app.com',
    'Connection': 'Keep-Alive'
    }
    name=''
    response = requests.request("GET", url, headers=headers, data=payload)
    name=response.text



    url = "https://api.eyecon-app.com/app/pic?cli="+str(new_query)+"&is_callerid=true&size=small&type=0&cancelfresh=0&cv=vc_365_vn_3.0.365_a"

    payload={}
    headers = {
        'e-auth-v': 'e1',
        'e-auth': 'b9b05235-7853-4e65-b8e8-015ff984d700',
        'e-auth-c': '33',
        'e-auth-k': 'PgdtSBeR0MumR7fO',
        'e-checksum': 'ZIv8tFHeLh4aVa0c7nu3/DMLtKc=',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
        'Host': 'api.eyecon-app.com',
        'Connection': 'Keep-Alive'
    }
    responses = requests.request("GET", url, headers=headers, data=payload)
    facebook_image=''
    try:
        facebook_image=responses.url
    except:pass
    facebook_url=''
    if 'api.eyecon-app.com' not in facebook_image:
        for x in responses.history:
            facebook_url=facebook_image
            if 'graph' in facebook_url:
                # fb_url=facebook_url.replace("graph.","").replace("picture?width=600&access_token=1716660955257637%7Cafa3f38c55f24c73b177afc962969b15","").replace("picture?width=200&access_token=1716660955257637%7Cafa3f38c55f24c73b177afc962","")
                
                id = re.findall('\d+', facebook_url)
                fb_id = id[0]
                fb_url = 'https://facebook.com/'+str(fb_id)
                if fb_id:
                    # Detail

                    url = "https://graph.facebook.com/graphql?locale=en_US"
                    
                    user_id=str(fb_id)

                    payload='access_token=EAAAAUaZA8jlABAC7eLwnoZAZBzCpIBkfgxZBnWxu2NRwNDi7FGUUDHbEs3Yn2uGb3lBeTxTt4l4YNdZAvES3cAquCFsViGTlIzadYtLsZCFz7PcOKQd1zQ1d3vT2t45amET2iqJPMxY4NH5BF5rdO1katO3x0LcPgIbIRvB4SpDNvh1YhEG08W&fb_api_caller_class=RelayModern&variables=%7B%22profileID%22%3A%22'+user_id+'%22%2C%22disableEdit%22%3Afalse%2C%22fieldSectionItemCount%22%3A25%2C%22scale%22%3A1.5%2C%22showAppSection%22%3Atrue%2C%22showLifeEvent%22%3Atrue%2C%22timelineSectionCount%22%3Anull%2C%22yearOverviewCount%22%3Anull%7D&doc_id=1940628916012542'
                    headers = {
                    'authority': 'graph.facebook.com',
                    'content-type': 'application/x-www-form-urlencoded',
                    'user-agent': 'Dalvik/2.1.0 (Linux; U; Android 7.1.2; SM-G965N Build/NRD90M) [FBAN/FB4A;FBAV/175.0.0.40.97;FBPN/com.facebook.katana;FBLC/en_US;FBBV/111983758;FBCR/null;FBMF/samsung;FBBD/samsung;FBDV/SM-G965N;FBSV/7.1.2;FBCA/x86:armeabi-v7a;FBDM/{density=1.5,width=720,height=1280};FB_FW/1;]',
                    'x-fb-net-hni': '31002',
                    'x-fb-http-engine': 'Liger',
                    }

                    try:
                        response = requests.request("POST", url, headers=headers, data = payload)
                    except Exception as x:
                        print("[-]  Issue In facebook Detail Response   ::  "+str(x))
                    if response:
                        res=json.loads(response.text)
                        work = []
                        education = []
                        current_city =[]
                        hometown=[]
                        socialmedia_apps = {}

                        for x in res.get('data').get('user').get('profile_field_sections').get('edges'):
                            fieldtype=(x.get('node').get('profile_fields').get('edges'))
                            for a in fieldtype:
                                type_work=(a.get('fieldInfo').get('field_type'))
                                if type_work == 'work':
                                    title=(a.get('fieldInfo').get('title').get('text'))
                                    work.append(title)
                                elif type_work == 'education':
                                    title=(a.get('fieldInfo').get('title').get('text'))
                                    education.append(title)
                                elif type_work == 'current_city':
                                    title=(a.get('fieldInfo').get('title').get('text'))
                                    current_city.append(title)
                                elif type_work == 'hometown':
                                    title=(a.get('fieldInfo').get('title').get('text'))
                                    hometown.append(title)
                                elif type_work == 'screenname':
                                    list_item=(a.get('fieldInfo').get('list_item_groups'))
                                    for socialmedia in list_item:
                                        list =socialmedia.get('list_items')
                                        for name in list:
                                            app_name = name.get('text').get('text')

                                    title=(a.get('fieldInfo').get('title').get('text'))
                        
                                    dict = {app_name:title}
                                    socialmedia_apps.update(dict)
                                    
                                

                        fb_detail_dict = {'Work':work,'Education':education,'current_city':current_city,'hometown':hometown,'Social_media':socialmedia_apps}
                        # print(fb_detail_dict)
                                
                try:
                    dict={"app_name":'Facebook','name':name,'fb_url':fb_url,'image':facebook_image}
                    print(Fore.BLUE+str(dict))
                    print(Fore.BLUE+str(fb_detail_dict))

                    return (dict,fb_detail_dict)
                except Exception as ex:
                  print("[-]  Response Issue    ::  ",str(query),str(ex))
    else:
        pass

def dubsmash(query):
  url = "https://gateway-production.dubsmash.com/graphql?build_number=46148&platform=android"

  payload = "{\r\n    \"operationName\": \"LoginUserMutation\",\r\n    \"query\": \"mutation LoginUserMutation($input: LoginUserInput!) {  loginUser(input: $input) {    __typename    user {      __typename      ...LoggedInUserGQLFragment    }    access_token    refresh_token    token_type  }}fragment LoggedInUserGQLFragment on PrivateUser {  __typename  uuid  username  first_name  last_name  display_name  profile_picture  country  language  num_posts  num_saved_videos  num_follows  num_followings  num_videos  num_videos_total  date_joined  share_link  num_invites_sent  badges  emails {    __typename    is_primary    is_verified    email  }  active_cultural_selections {    __typename    code    flag_icon    language_name  }  phones {    __typename    is_primary    is_verified    phone  }  allow_video_download}\",\r\n    \"variables\": {\r\n        \"input\": {\r\n            \"client_id\": \"n9Hbf8srKdTy6nZwqyy3=u9d)u6LYqmovMysHUda%KPUF\",\r\n            \"client_secret\": \"aByCWW[yewvDWQXFbTyRPZ7ynkvv6Em>PQ2h9J+L2Tvo7\",\r\n            \"grant_type\": \"PASSWORD\",\r\n            \"password\": \"dubsmash123#@!\",\r\n            \"username\": \"shanzasohail2020@gmail.com\"\r\n        }\r\n    }\r\n}"
  headers = {
    'x-apollo-operation-id': '13c08ab781da52c9eda19a43f92514b7a498c601b7c6d6343b36e0aac008ed62',
    'x-apollo-operation-name': 'LoginUserMutation',
    'x-dmac': 'b4267454e2f64e59c4e5e3dd3ec5e89b4a0248f175281e0c97689fdb7d5b8700',
    'x-dmac-version': '2',
    'x-device-country': 'US',
    'x-time': '1610089692',
    'x-device-timezone': '-28800',
    'x-build-number': '46148',
    'x-app-version': '5.4.0',
    'x-device-language': 'en',
    'x-dubsmash-device-id': '009199b9-1e5e-44e3-98ea-c026ad2c1a4a',
    'x-platform': 'android',
    'accept-language': 'en',
    'content-type': 'application/json; charset=utf-8',
    'Cookie': '__cfduid=df2cc65527fb031bf34b077477b6515991615971411'
  }

  try:
    response = requests.request("POST", url, headers=headers, data=payload)
  except Exception as x:
    print('[-]  Issue in Response ! Dubsmash' + str(x))
  res=response.json()
  aceess_token=res.get('data').get('loginUser') .get('access_token')
  # print(aceess_token)

  url = "https://gateway-production.dubsmash.com/graphql?build_number=46148&platform=android"

  payload = json.dumps({
    "operationName": "SearchContentQuery",
    "variables": {
      "term": str(query),
      "searchTypes": [
        "USER",
        "TAG",
        "SOUND",
        "PROMPT"
      ],
      "page": None
    },
    "query": "query SearchContentQuery($term: String!, $searchTypes: [SearchType!]!, $page: Int) {  search(term: $term, search_types: $searchTypes, page: $page) {    __typename    total_results    results {      __typename      data {        __typename        ... on Sound {          ...RichSoundGQLFragment        }        ... on Prompt {          ...RichPromptGQLFragment        }        ... on User {          ...RichUserGQLFragment        }        ... on Tag {          ...TagBasicsGQLFragment        }      }    }    next_page  }}fragment RichSoundGQLFragment on Sound {  __typename  ...SoundBasicsGQLFragment  top_videos {    __typename    ... TopVideoGQLFragment  }}fragment SoundBasicsGQLFragment on Sound {  __typename  uuid  sound_data: sound  name  sound_waveform_raw_data: waveform_raw_data  liked  creator {    __typename    ...CreatorUserGQLFragment  }  created_at  share_link  num_videos  num_likes}fragment TopVideoGQLFragment on Video {  __typename  uuid  video_data {    __typename    mobile {      __typename      thumbnail    }  }  sound {    __typename    uuid  }  prompt {    __typename    uuid  }}fragment CreatorUserGQLFragment on User {  __typename  uuid  username  display_name  profile_picture  share_link  date_joined  followed  badges}fragment RichPromptGQLFragment on Prompt {  __typename  ...PromptBasicsGQLFragment  top_videos {    __typename    ... TopVideoGQLFragment  }}fragment PromptBasicsGQLFragment on Prompt {  __typename  uuid  type  created_at  name  creator {    __typename    ...CreatorUserGQLFragment  }  liked  share_link  num_likes  num_videos}fragment RichUserGQLFragment on User {  __typename  ...UserBasicsGQLFragment  top_videos {    __typename    ... TopVideoGQLFragment  }}fragment UserBasicsGQLFragment on User {  __typename  uuid  username  display_name  profile_picture  num_posts  num_follows  num_followings  followed  blocked  share_link  date_joined  num_videos  badges  allow_video_download}fragment TagBasicsGQLFragment on Tag {  __typename  name  num_objects}"
  })
  headers = {
    'x-apollo-operation-id': '12a47bba9c0bebec6e149be940d7c83dc05e6ecf22606a26e37d599e5d00817a',
    'x-apollo-operation-name': 'SearchContentQuery',
    'x-apollo-cache-key': 'bdff0d1072d818ec3679e9c8db724892',
    'x-apollo-cache-fetch-strategy': 'NETWORK_FIRST',
    'x-apollo-expire-timeout': '3600000',
    'x-apollo-expire-after-read': 'false',
    'x-apollo-prefetch': 'false',
    'x-apollo-cache-do-not-store': 'false',
    'x-dmac': '2c5c033e03e9dd63b32763370221d94b45a0174f0d701e3d9ec854f3a457f7b4',
    'x-dmac-version': '2',
    'x-device-country': 'GB',
    'x-time': '1589962201',
    'x-device-timezone': '18000',
    'x-build-number': '45516',
    'x-app-version': '5.3.2',
    'x-accept-content-language': 'en_PK',
    'x-device-language': 'en',
    'x-dubsmash-device-id': 'a3aa7b09-5d2a-42e4-ada8-1eeeac4ff95c',
    'x-platform': 'android',
    'authorization': 'Bearer '+aceess_token,
    'Content-Type': 'application/json',
    'Cookie': '__cfduid=df2cc65527fb031bf34b077477b6515991615971411'
  }

  response = requests.request("POST", url, headers=headers, data=payload)
  res=response.json()
  results=res.get('data').get('search').get('results')
  for data in results:
    user_info=data.get('data')
    dict = {'app_name':'dubsmash','user_info':user_info}
    print(Fore.RED+str(dict))

def syncme(query):
  url = "https://api.sync.me/api/premium/webview"

  payload="{\"ACCESS_TOKEN\":\"FrhGvrQhPLnf9sNkZhZdRP\",\"APPLICATION_ID\":\"8a078650-5acd-11e1-b86c-0800200c9a66\",\"APPLICATION_VERSION\":\"4.18.15\",\"phone\":\""+str(query)+"\",\"locale\":\"en\",\"js_events\":true}"
  headers = {
    'Content-Type': 'application/json'
  }

  response = requests.request("POST", url, headers=headers, data=payload)
  html=response.text.replace('\n','')
  soup = BeautifulSoup(html,"html.parser")
  try:
    name = soup.find("h1", {"class":'profile-name'}).text
  except:
    print('no name ')
  try:
    images = soup.findAll('img')

    for x in images:
      user_image = x['src']
      if 'syncmeapp.com' in user_image:
        img = user_image
      else:
        img = '[-]  No image found'
    return name,img
  except Exception as x:
    print('[-]  Image not Found At  ::   '+str(query)+'Exception ::  '+str(x))

def fitnesspal_token():
    # TOKEN
    url = "https://identity.api.ua.com/oauth/token"

    payload='client_id=1c70aed5-15c7-40a2-b4f0-a55ed1a5c43c&client_secret=7xilqzoa2lqngjgi7vilqaqygq64cgbmc7pmsf4onvfelatb6vla&grant_type=client_credentials'
    headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    token_json = response.json()

    access_token = token_json.get('access_token')
    # data = {'fitnesspal_token':str(access_token)}

    config_file = open("config.json", "r")
    json_object = json.load(config_file)
    
    json_object["fitnesspal_token"] = str(access_token)

    config_file = open("config.json", "w")
    json.dump(json_object, config_file)
    config_file.close()

    # with open("config.json", "w") as jsonfile:

    #     json_object = json.load(jsonfile)
        

    #     access_token = json.dump(data, jsonfile) # Writing to the file
    #     jsonfile.close() 
    return access_token

def fitness_pal(query,access_token):

    url = "https://identity.api.ua.com/users/search?emailAddress="+str(query)
    payload={}
    headers = {
    'api-key': 'juuqn3eqmq9qecrc3yvgdkme9gvyv4be',
    'authorization': 'Bearer  '+str(access_token)
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    email_res_json = response.json()
    try:
        if response.status_code == 401:
            access_token =fitnesspal_token()
            fitness_pal(query,access_token)
                
    except:
        pass
    
    for data in email_res_json:
        try:
            userId = data.get('userId')
            if userId:
                url = "http://identity.api.ua.com/profiles/"+str(userId)

                payload={}
                headers = {
                'api-key': 'juuqn3eqmq9qecrc3yvgdkme9gvyv4be',
                'authorization': 'Bearer '+str(access_token)
                }

                response = requests.request("GET", url, headers=headers, data=payload)
                res_json = response.json()
                try:
                    del res_json['_links']
                except:
                    pass
                dict = {'app_name':'Fitness Pal','user_info':res_json}
                print(Fore.GREEN+str(dict))
                return dict
        except:pass



def yelp(query):
    url = "https://auto-api.yelp.com/user/friend_finder_v2?time=1628669842&nonce=JCixow%3D%3D&ywsid=Y3yWooClkisSbx32yJG5Ww&device_type=samsung%2Bdream2lteks%2FNRD90M.G955NKSU1AQDC&app_version=21.11.0-21211123&cc=US&lang=en&efs=olRnkmGp7GlVd6TWLBX0Raulb51ipeVVnTfmKSBOiJYwRL3U%2F5FyGJhKyqJcxLNChcNVx9oZqFgYgrvekXA78eh0nj3AOs4Ygt2VfjwgozU%3D&signature=_BsqG4nJRSMjJyTHLAPsSTBU5UTs%3D"
    payload='emails='+str(query)+'&ignored=false&include_facebook=false'
    headers = {
    'authority':'auto-api.yelp.com',
    'x-auth-token-2': 'NU8DCFmYTrA1Ek-69T0ZrsSVkoUTYQ',
    'x-screen-scale': '1.5',
    'x-foregrounded': 'true',
    'user-agent': 'Version/1 Yelp/v21.11.0-21211123 Carrier/Android Model/dream2lteks OSBuild/NRD90M.G955NKSU1AQDC Android/7.1.2',
    'Content-Type': 'application/x-www-form-urlencoded',
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    res_json =  response.json()
    try:
        del res_json['message']
    except:pass
    dict = {'app_name':'Yelp','user_info':res_json}
    print(Fore.YELLOW+str(dict))  
    return dict

def trello(query):
    url = "https://trello.com/1/search/members?idBoard=610d00a391914c44b00ee8c6&idOrganization=610d00a285692e8918c6140a&onlyOrgMembers=false&onlyManagedMembers=false&onlyOrgOrManagedMembers=false&query="+str(query)
    payload={}
    headers = {
    'authorization': 'OAuth oauth_consumer_key="09a8d894fc0cdf43185f9ffec43457ca", oauth_token="fd6be262e7cc180daa351e336bd03ee12fde809b0a12a6d11c1cd13b239b4d82"',
    }
    response = requests.request("GET", url, headers=headers, data=payload)
    res_trello_json = response.json()
    dict = {'app_name':'Trello','user_info':res_trello_json}
    print(Fore.MAGENTA+str(dict))
    return dict

def lomotif(query):
    url = "https://app.lomotif.com/v1/user/search/?source=contacts"
    payload = "{\r\n    \"emails\":[\r\n        \""+str(query)+"\"\r\n        \r\n    ]\r\n}"
    headers = {
    'authority':'app.lomotif.com',
    'x-country-code': 'PK',
    'user-agent': 'Client/Android/2.7.4/7.1.2',
    'authorization': 'Token 3b929694738a6129e4ef38575007bf13ed0dfc33',
    'accept-language': 'en-US',
    'x-user-id': 'e61ecd2c-c09c-4738-a917-6f0d1828f895R',
    'x-lomotif-agent': 'Client/Android/2.7.4/7.1.2',
    'content-type': 'application/json; charset=UTF-8'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    res_lomotif_json = response.json()
    try:
        user_info = res_lomotif_json.get('results')
    except:
        pass
    dict = {'app_name':'Lomotif','user_info':user_info}
    print(Fore.RED+str(dict))
    return dict

def runkeeper(query):
    url = "https://fitnesskeeperapi.com/RunKeeper/deviceApi/usersForFbuids?timeZoneStr=America%2FPhoenix&device=android%2CSM-G955N%2C7.1.2%2C25%2CSM-G955N&maxWorkoutUnitsVersion=0&deviceID=7453e4f3-80fb-4bfc-8497-e221ff3b5a47&email=farhans.ali.ch%40gmail.com&utcOffsetSec=-25200&deviceApp=paid%2C11.9.1.14302&maxWorkoutPaceVersion=2&apiVer=2.3"
    payload='emails=%20%5B%22'+str(query)+'%22%5D&fbuids=%20%5B%5D'

    headers = {
    'authorization': 'Bearer 3f9c68f5-407c-44c0-8f18-6423c2c30cca',
    'Content-Type': 'application/x-www-form-urlencoded'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    res_runkeeper_json = response.json()
    dict = {'app_name':'RunKeeper','user_info':res_runkeeper_json}
    print(Fore.CYAN+str(dict))

def anghami(query):
    # import pdb;pdb.set_trace()
    url = "https://api.anghami.com/gateway.php?type=GETtabsearch&output=jsonhp&query="+str(query)+"&edge=false&musiclanguage=1&searchtype=user&page=0&count=&uiv=2&lang=en&language=en&fingerprint=5bb96d31-965c-4d3e-8742-2afc72081b7c&web2=true&sid=i7:lelhjdeg::ececdcdfdcchgc:CX:d:ra:j:9:::0:na:5s60884oq8&angh_type=GETtabsearch"

    payload={}
    headers = {
    'Accept': 'application/json, text/plain, */*',
    'Referer': 'https://play.anghami.com/search/scottbilla@gmail.com/user',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36',
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    try:
        res_json_anghami = response.json()
        if res_json_anghami:
            sections = res_json_anghami.get('sections')
            for info in sections:
                user_info = info.get('data')
            if user_info:
                dist = {'app_name':'Anghami','user_info':user_info}
                print(Fore.RED+str(dist))
                return dist  
    except:
        print('user not found')  

def bookmate(query):

    a_file = open("config.json", "r")
    json_object = json.load(a_file)
    token =  json_object.get('Bookmate_token')

    url = "https://api.bookmate.rocks/a/4/u/contacts"
    if '@' in query:
        payload='contacts%5B%5D%5Bemails%5D='+str(query)
    else:
        payload='contacts%5B%5D%5Bphones%5D='+str(query)
    headers = {
    'Authorization': str(token),
    'Content-Type': 'application/x-www-form-urlencoded'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    try:
        bookmate_json = response.json()
        # import pdb;pdb.set_trace();
        if bookmate_json:
            try:
                error = bookmate_json.get('error')
                if error:
                    bookmate_token()
                    bookmate(query)
                
            except:
                bookmate_user_info = bookmate_json
                dict_bookmate = {'appname':'Bookmate','user_info':bookmate_user_info}
                print(Fore.YELLOW+str(dict_bookmate))
                return dict_bookmate

    except:
        
        bookmate_token()
        bookmate(query)
        
def bookmate_token():

    url = "https://api.bookmate.rocks/api/v5/sign_in/session"
    payload={}
    headers = {
    'App-User-Agent': 'samsung/SM-G955N Android/7.1.2 Bookmate/6.13',
    'MCC': '310',
    'MNC': '02',
    'IMEI': '21dd6f4acbd9d3da',
    'Subscription-Country': 'PK',
    'App-Locale': 'en',
    'Bookmate-Version': '20200305',
    'Bookmate-Websocket-Version': '20190601',
    'Device-Idfa': '5417a91f-0dc1-4eb6-ac99-15e41d9f385e',
    'Appsflyer-ID': '1631706717003-3311241775668496542',
    'Auth-Token': 'eggkzQeFFsXC1qOFHgrga0xTFGB6azjK',
    'Host': 'api.bookmate.rocks',
    'Connection': 'Keep-Alive'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    try:
        res_bookmate_token_json = response.json()
        bookmate_token = res_bookmate_token_json.get('token').get('value')

        if bookmate_token:

            try:
                a_file = open("config.json", "r")
                json_object = json.load(a_file)
                
                json_object["Bookmate_token"] = str(bookmate_token)

                a_file = open("config.json", "w")
                json.dump(json_object, a_file)
                a_file.close()

            except Exception as x :print(x)

    except Exception as x :print(x)


def get_okapp_token():
    username='923304504298'
    url = "https://api.ok.ru/api/batch/executeV2"

    payload='application_key=CBAFJIICABABABABA&id=auth.login&methods=%5B%7B%22auth.login%22%3A%7B%22params%22%3A%7B%22client%22%3A%22android_8_19.6.10%22%2C%22deviceId%22%3A%22INSTALL_ID%3Db0f5297c-4d37-4010-8a4a-242e40136a95%3BDEVICE_ID%3D351564201268182%3BANDROID_ID%3Db9b7b1988ef0e5ac%3B%22%2C%22gen_token%22%3Atrue%2C%22password%22%3A%22PAKISTAN1947%22%2C%22user_name%22%3A%22'+username+'%22%2C%22verification_supported%22%3Atrue%2C%22verification_supported_v%22%3A%221%22%7D%7D%7D%2C%7B%22settings.get%22%3A%7B%22params%22%3A%7B%22keys%22%3A%22*%22%2C%22marker%22%3A%220%22%2C%22version%22%3A519%7D%2C%22onError%22%3A%22SKIP%22%7D%7D%2C%7B%22libverify.libverifyPhoneActual%22%3A%7B%22onError%22%3A%22SKIP%22%7D%7D%2C%7B%22users.getCurrentUser%22%3A%7B%22params%22%3A%7B%22client%22%3A%22android_8_19.6.10%22%2C%22fields%22%3A%22uid%2Clocale%2Chas_phone%2Cbirthday%2Ccurrent_location%22%7D%2C%22onError%22%3A%22SKIP%22%7D%7D%2C%7B%22users.getInfo%22%3A%7B%22params%22%3A%7B%22client%22%3A%22android_8_19.6.10%22%2C%22emptyPictures%22%3A%22true%22%2C%22fields%22%3A%22first_name%2Cage%2Cpic_base%2Clast_name%2Cvip%2Ccan_vmail%2Cregistered_date_ms%2Conline%2Cpic_full%2Cgender%2Cshow_lock%2Cbirthday%2Clocation%2Cpremium%2Cpic600x600%2Cpic190x190%2Cphoto_id%22%7D%2C%22supplyParams%22%3A%7B%22uids%22%3A%22users.getCurrentUser.uid%22%7D%2C%22onError%22%3A%22SKIP%22%7D%7D%2C%7B%22presents.getActive%22%3A%7B%22params%22%3A%7B%22fields%22%3A%22present_type.*%2Cpresent_type.has_surprise%2Cpresent.*%22%7D%2C%22supplyParams%22%3A%7B%22fid%22%3A%22users.getCurrentUser.uid%22%7D%2C%22onError%22%3A%22SKIP%22%7D%7D%5D'
    headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    res=response.json()

    for x in res:
        data=x.get('ok')
        session_key=(data.get('session_key'))
        return session_key


def ok_search(query):

    session = get_okapp_token()
    
    url = "https://api.ok.ru/api/search/byContactsBook"

    if '@' in query:
        payload='__screen=feed_main%2Cfriends&application_key=CBAFJIICABABABABA&session_key='+str(session)+'&fields=user.last_name%2Cuser.gender%2Cuser.age%2Cuser.uid%2Cuser.first_name%2Cuser.pic600x600%2Cuser.online%2Cuser.pic128x128%2Cuser.location&query=%7B%22credentials%22%3A%5B%7B%22firstName%22%3A%22fdfs%22%2C%22phone%22%3A%22923304504302%22%2C%22hasAvatar%22%3Afalse%2C%22hasRingtone%22%3Afalse%2C%22isFavorite%22%3Afalse%7D%2C%7B%22firstName%22%3A%22das%22%2C%22email%22%3A%22'+str(query)+'%22%2C%22hasAvatar%22%3Afalse%2C%22hasRingtone%22%3Afalse%2C%22isFavorite%22%3Afalse%7D%5D%7D'
    else:
        payload='__screen=feed_main%2Cfriends&application_key=CBAFJIICABABABABA&session_key='+str(session)+'&fields=user.last_name%2Cuser.gender%2Cuser.age%2Cuser.uid%2Cuser.first_name%2Cuser.pic600x600%2Cuser.online%2Cuser.pic128x128%2Cuser.location&query=%7B%22credentials%22%3A%5B%7B%22firstName%22%3A%22fdfs%22%2C%22phone%22%3A%22'+str(query)+'%22%2C%22hasAvatar%22%3Afalse%2C%22hasRingtone%22%3Afalse%2C%22isFavorite%22%3Afalse%7D%5D%7D'
    headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
    }
    try:
        response = requests.request("POST", url, headers=headers, data=payload)
        if response.status_code == 200:
            ok_json = response.json()
            ok_user_info = ok_json.get('users')
            dict_ok = {'appname':'OK','user_info':ok_user_info}
            print(Fore.MAGENTA+str(dict_ok))
    except Exception as x:
        print('[-]  OK issue  :: '+str(x))


@app.get("/email/{query}")
async def All_Applications_Data(query):
    if '@' in query:
        try:
            twitter(query)
            a = twitter(query)

        except Exception as x:
            print('[-]  Twitter Exception   ::  '+str(x))
        try:
            skype_request_flow(query)
            b = skype_request_flow(query)
        except Exception as x:
            print('[-]  Skype Exception   ::  '+str(x))
        try:
            strava(query)
            c = strava(query)

        except Exception as x:
            print('[-]  Strava Exception   ::  '+str(x))
        try:
            weheartit(query)
            d = weheartit(query)
        except Exception as x:
            print('[-]  Weheartit Exception   ::  '+str(x))
        try:
            foursquare(query)
            e = foursquare(query) 
        except Exception as x:
            print('[-]  Foursquare Exception   ::  '+str(x))
        try:
            addidas(query)
            f =  addidas(query)
        except Exception as x:
            print('[-]  Addidas Exception   ::  '+str(x))
        try:
            nike(query)
            g = nike(query)
        except Exception as x:
            print('[-]  Nike Exception   ::  '+str(x))
        try:
            fit_bit(query)
            h = fit_bit(query)
        except Exception as x:
            print('[-]  Fit_bit Exception   ::  '+str(x))
        try:
            duolingo(query)
            i = duolingo(query)
        except Exception as x:
            print('[-]  Duolingo Exception   ::  '+str(x))
        # try:
        #     dubsmash(query)
        # except Exception as x:
        #     print('[-]  Dubsmash Exception   ::  '+str(x))

        try:
            with open("config.json", "r") as jsonfile:
                json_file = json.load(jsonfile)
                access_token = json_file.get('fitnesspal_token')
                jsonfile.close()
            fitness_pal(query,access_token)
            j = fitness_pal(query,access_token)
        except Exception as x:
            print('[-]  FitnessPal Exception   ::  '+str(x))
        try:
            yelp(query)
            k = yelp(query)
        except Exception as x:
            print('[-]  Yelp Exception   ::  '+str(x))
        try:
            trello(query)
            l = trello(query)
        except Exception as x:
            print('[-]  Trello Exception   ::  '+str(x))
        try:
            lomotif(query)
            m = lomotif(query)
        except Exception as x:
            print('[-]  Lomotif Exception   ::  '+str(x))
        
        try:
            runkeeper(query)
            n = runkeeper(query)
        except Exception as x:
            print('[-]  Runkeeper Exception   ::  '+str(x))
        try:
            anghami(query)
            o = anghami(query)
        except Exception as x:
            print('[-]  Anghami Exception   ::  '+str(x))
        # try:
        #     olx(query)
        # except Exception as x:
        #     print('[-]  Olx Exception   ::  '+str(x))
        try:
            bookmate(query)
            p = bookmate(query)
        except Exception as x:
            print('[-]  Bookmate Exception   ::  '+str(x))
        
        # try:
        #     ok_search(query)
        # except Exception as x:
        #     print('[-]  OK  Exception   ::  '+str(x))

        return a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p

@app.get("/phone/{query}")

async def All_Applications_Data(query) : 
    if '+' or '92' in query:
        country_code='92'
        # try:
        #     twitter(query)
        # except:
        #     pass
        try:
            number(query)
            res = number(query)
        except:
            pass
        try:
            view_caller(query)
            res2 = view_caller(query)
        except:
            pass
        try:
            miCaller(query)
            res3 = miCaller(query)
        except Exception as x:
            print('[-]  Mi-Caller Exception   ::  '+str(x))
        
        try:
            skype_request_flow(query)
            res4 = skype_request_flow(query)
        except:
            pass
        try:
            foursquare(query)
            res5 = foursquare(query)

        except:
            pass
        try:
            syncme(query)
            res6 = syncme(query)

        except:
            pass
        try:
            eyecon(query)
            res7 = eyecon(query)

        except Exception as x:
            print('[-]  Facebook Exception   ::  '+str(x))

        try:
            if query.startswith('+92'):
                tw_query = query[3:]
            true_caller(tw_query)
            res8 = true_caller(tw_query)

        except:
            pass
        try:
            if query.startswith('+92'):
                who_query = query[1:]
            whosCalling(who_query)
            res9 = whosCalling(who_query)

        except:
            pass
        # try:
        #     olx(query)
        # except Exception as x:
        #     print('[-]  Olx Exception   ::  '+str(x))

        try:
            bookmate(query)
            res10 = bookmate(query)

        except Exception as x:
            print('[-]  Bookmate Exception   ::  '+str(x))

        # try:
        #     ok_search(query)
        # except Exception as x:
        #     print('[-]  OK  Exception   ::  '+str(x))
        return res,res2,res3,res4,res5,res6,res7,res8,res9,res10       
    else:
        print('[sss]')


# BOOKMATE REMAINING      

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8013)
# if __name__ == "__main__":
#     query=''
#     All_Applications_Data(query)



