import requests
import uvicorn
from fastapi import FastAPI

app = FastAPI()
@app.get("/query/{phone}")

async def test(phone):
    if '@' in phone:
        url = "http://50.116.35.166/twtr/twlkup/getanatwlup.php?type=email&email="+str(phone)+"&map=true"
    else:
        url = "http://50.116.35.166/twtr/twlkup/getanatwlup.php?type=phone&phone="+str(phone)+"&map=true"

    payload={}
    headers = {}

    response = requests.request("GET", url, headers=headers, data=payload)
    res =response.json()

    return res

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8011)