import requests
import uvicorn
from fastapi import FastAPI

app = FastAPI()
@app.get("/query/{number}")

async def test(number):
    # import pdb;pdb.set_trace()
    if number.startswith('+92'):
        search_number = number[3:]
    url = "https://acr2.y0.com/identify"
    payload="[{\"number\":"+search_number+",\"prefix\":92}]"
    headers = {
        'Content-Type': 'application/json',
        'Cookie': '__cfduid=d162f2af6a87dd4a604eda0bfae1ca1691611904868'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    
    try:
        if response.status_code==200:
            json_response=response.json()
            new_name=''
            name_list=[]
            for names in json_response:
                new_name=names.get('names')
            for multiple_names in new_name:
                name=multiple_names.get('name')
                name_list.append(name)
                print(name_list)
            return name_list
            
    except Exception as ex:
        print("[-]  No detail Found ::  ",str(ex))

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8011)