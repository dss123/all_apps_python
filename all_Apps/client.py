import time
import grpc
import argparse
import all_Apps_pb2
import all_Apps_pb2_grpc


def run(link):
    channel = grpc.insecure_channel(target = "localhost:50014")
    stub = all_Apps_pb2_grpc.allAppsStub(channel)
    res = stub.GetallApps(all_Apps_pb2.GetQuery(query = link))
    
    print(res)
    

if __name__ == "__main__":
    # run("+923149515553")
    # run("rizwansudhozai@gmail.com")
    run("scottbilla@gmail.com")
    # run('+923304504302')
    # run("sohaib230@gmail.com")
    # run("zeeshanzakaria@hotmail.com")
