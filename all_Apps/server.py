
import json
import time
import grpc
import all_Apps_pb2
import all_Apps_pb2_grpc
from concurrent import futures
from datetime import datetime as d
import all_Applications 
final = {}

class All_Applications(all_Apps_pb2_grpc.allAppsServicer):
    def GetallApps(self , req , context):
        st = d.now()
        req = req.query
        print("New Requests:    " + req)

        r=all_Applications.All_Applications_Data(req)        
        print("total time:   " + str(d.now() - st))
        # return all_Apps_pb2.SendResponse(response = json.dumps(r))
        return all_Apps_pb2.SendResponse(response = r)


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1))
    all_Apps_pb2_grpc.add_allAppsServicer_to_server(All_Applications() , server)
    server.add_insecure_port('0.0.0.0:50014')
    server.start()
    print ("All Apps Started!!!")
    try:
        while True:
            time.sleep(3600)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == "__main__":
    serve()